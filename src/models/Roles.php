<?php

class Roles
{
    private Integer $id_roles;
    private String $name;


    public function __construct(int $id_roles, string $name)
    {
        $this->id_roles = $id_roles;
        $this->name = $name;
    }

    public function getIdRoles(): int
    {
        return $this->id_roles;
    }


    public function setIdRoles(int $id_roles): void
    {
        $this->id_roles = $id_roles;
    }


    public function getName(): string
    {
        return $this->name;
    }


    public function setName(string $name): void
    {
        $this->name = $name;
    }


}