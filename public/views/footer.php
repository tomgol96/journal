<!DOCTYPE HTML>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="public/css/footer.css">


</head>
<body>
<footer>
    <div class="footer">
        <ul>
            <li><strong>Kontakt:</strong> ul. Firmowa 2a, 12-345 Miasto</li>
            <li class="center"><strong>Telefon:</strong> 12 345-67-89</li>
            <li><strong>Mail:</strong><a href="mailto:mail@mail.pl">mail@mail.pl</a></li>

        </ul>
    </div>
</footer>
</body>
