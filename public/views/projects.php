<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/projects.css">

    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <title>Dziennik</title>
</head>
<?php
require('header.php');
?>
<body>
<div class="base-container">
    <?php
    require('sidebar.php');
    ?>
    <main>
        <header>
            <div class="search-bar">
                <form>
                    <input placeholder="szukaj zdarzeń">
                </form>
            </div>
            <div class="add-project">
                <a href="addProject">">
                <i class="fas fa-plus"></i> Dodaj zdarzenie
            </div>
        </header>
        <section class="projects">
            <?php foreach($projects as $project): ?>
                <div id="project-1">
                    <img src="public/uploads/<?= $project->getImage(); ?>">
                    <div>
                        <h2><?= $project->getTitle(); ?></h2>
                        <p><?= $project->getDescription(); ?></p>

                    </div>
                </div>
            <?php endforeach; ?>
        </section>
    </main>
</div>
</body>
<?php
    require('footer.php');
    ?>
</body>
