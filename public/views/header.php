<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="public/css/header.css">


</head>
<body>
<div class="header">
    <a href="projects" class="logo"><img src="public/img/logo.svg" width="150px"height="73px">Dziennik</a>
    <div class="header-right">
        <a class="active" href="projects">Strona główna</a>
        <a href="#contact">Moje konto</a>
        <a href="logout.php">Wyloguj</a>
    </div>
</div>
</body>
</html>
