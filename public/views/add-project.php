<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/projects.css">
    <link rel="stylesheet" type="text/css" href="public/css/add-project.css">

    <script src="https://kit.fontawesome.com/723297a893.js" crossorigin="anonymous"></script>
    <title>PROJECTS</title>
</head>
<?php
require('header.php');
?>
<body>
<div class="base-container">
    <?php
    require('sidebar.php');
    ?>
    <main>
        <header>
            <div class="search-bar">
                <form>
                    <input placeholder="search project">
                </form>
            </div>

        </header>
        <section class="project-form">
            <h1>Wyślij plik</h1>
            <form action="addProject" method="POST" ENCTYPE="multipart/form-data">
                <div class="messages">
                    <?php
                    if(isset($messages)){
                        foreach($messages as $message) {
                            echo $message;
                        }
                    }
                    ?>
                </div>
                <input name="title" type="text" placeholder="Nazwa trasy">
                <textarea name="description" rows=5 placeholder="Opis"></textarea>

                <input type="file" name="file"/><br/>
                <button type="submit">send</button>
            </form>
        </section>
    </main>
</div>
<?php
require('footer.php');
?>
</body>